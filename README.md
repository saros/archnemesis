Archnemesis is a literate programming architecture DSL.
You can use this language to define constraints on
the architecture of your project. 

# Installation

Requires Maven 3.0.5 and JRE 1.7. 
In the root folder of the project type:
`mvn clean install`

# Architecture Compliance Checking

If your project uses Sonarqube, you can use this DSL together 
with https://github.com/saros-project/archnemesis-sonar-rule
for architecture compliance checking. Just make sure to add 
an architecture.chralx file to your project root.

# Eclipse Editor 

To edit a .chralx file, use the Eclipse Archnemesis
Editor, which can be installed through the following
update site:

http://saros-build.imp.fu-berlin.de/update-archnemesis/

# Syntax & Semantics

For more info on the syntax and semantics, please refer
to the primer.chralx in this folder.

